#include "main.h"

int lecture_du_score (int numniv){
    char* command;
    char* scorepath;
    int score;
	FILE* fichier;

    scorepath = malloc(sizeof(char)*30);
	sprintf(scorepath, "scores/scores_%d", numniv);

	fichier = fopen(scorepath, "r");
    if(fichier == NULL){
        command = malloc(sizeof(char)*30);
        sprintf(command, "touch scores/scores_%d", numniv);
        system(command);
        return -1;
    }
	
	fscanf(fichier, "%d", &score);

    fclose(fichier);

    return score;
}

void ecriture_du_score (int numniv, int score){
    char* scorepath;
	FILE* fichier;

    scorepath = malloc(sizeof(char)*30);
	sprintf(scorepath, "scores/scores_%d", numniv);

	fichier = fopen(scorepath, "w");

	// printf("Score à écrire : %d\n", score);
	fprintf(fichier, "%d", score);

	fclose(fichier);
}

char* nom_du_joueur (void){
    char* saisie;
    saisie = malloc(sizeof(char)*(8 + 1));
    printf("Quel pseudonyme veux-tu avoir dans le tableau des scores ? ");
    scanf("%8s", saisie);
    return saisie;
}

void ecriture_du_score_multi (int numniv, int score, char* nom) {
    int namefound;
    int linenum;
	int oldscore;
	int size; //Stock nb caractère dans un fichier
	char* commandaddline;
    char* commanddel;
    char* commandgrep;
    char* commandcut;
    char* scorepath;
    FILE* file; //Fichier score
    FILE* cutfile; //Recup résultat cut aka un num de ligne

	create_scores_files(numniv);
    // Trouve le chemin du fichier des scores
    scorepath = malloc(sizeof(char)*32);
	sprintf(scorepath, "scores/scores_multi_%d", numniv);

	oldscore = get_score_of(nom, scorepath);

    file = fopen(scorepath, "a"); //Ouvre le fichier scores_multi_nb en mode append
	fseek (file, 0, SEEK_END);
    size = ftell(file);
	// printf("Dans le fichier : %s \nIl y a %d caractères\n", scorepath, size);

	if(size > 0){
		commandgrep = malloc(sizeof(char)*128);
		sprintf(commandgrep, "cat %s |grep -n '%s'", scorepath, nom);
		// printf("Commandgrep : %s\n", commandgrep);
		namefound = system(commandgrep);
		// printf("Retour système : %d\n", namefound);

		commanddel = malloc(sizeof(char)*128);
		if(namefound == -1 || WEXITSTATUS(namefound) != 0){
			// printf("Le nom %s n'a pas été trouvé !\n", nom);
			// printf("Lignes : %d", get_line_number(scorepath));
			if(get_line_number(scorepath) >= 5){
				printf("Trop de lignes, trimage en cours");
				sprintf(commanddel, "sed -i '6,$ d' %s", scorepath);
			} 
		}else {
			commandcut = malloc(sizeof(char)*128);
			sprintf(commandcut, "%s |cut -d: -f1", commandgrep);
			cutfile = popen(commandcut, "r");
			fscanf(cutfile, "%d", &linenum);
			printf("Le nom %s a été trouvé à la ligne %d!\n", nom, linenum);
			if(oldscore > score){
				sprintf(commanddel, "sed -i '%d d' %s",linenum,scorepath);
				// printf("Commanddel : %s\n", commanddel);
				system(commanddel);
			}

			pclose(cutfile);
		}
	}
	if(oldscore > score || oldscore == -1){
		commandaddline = malloc(sizeof(char)*128);
		sprintf(commandaddline, "echo '%d %s' >> %s", score, nom, scorepath);
		system(commandaddline);
	}
	// fprintf(file, "%d %s", score, nom); //Car apparemment c'est trop demander d'append un fichier

    fclose(file);

    sort_scores(numniv);
}

void sort_scores(int numniv){
    char* command;
    char* scorepath;
	FILE* fichier;

    scorepath = malloc(sizeof(char)*32);
	sprintf(scorepath, "scores/scores_multi_%d", numniv);

	fichier = fopen(scorepath, "r");
    if(fichier == NULL) {
        return;
    }
    command = malloc(sizeof(char)*64);
    sprintf(command, "sort -no %s %s", scorepath, scorepath);
    system(command);

}


int in_top_5 (int numniv, int score){
    int fifthscore;
	int numlines;
    char* scorepath;
    char* commandscore;

	create_scores_files(numniv);
    sort_scores(numniv);

    scorepath = malloc(sizeof(char)*64);
	sprintf(scorepath, "scores/scores_multi_%d", numniv);

    commandscore = malloc(sizeof(char)*64);
	sprintf(commandscore, "cat %s|tail -n1", scorepath);

    fifthscore = score_of_fifth(commandscore);

	numlines = get_line_number(scorepath);

    // printf("Dernier score : %d\nscore : %d\n", fifthscore, score);

    if(score < fifthscore || numlines<5) return 1;
    else return 0;
}

int score_of_fifth(char* commandscore){
	FILE* file;
    int fifthscore;

    file = popen(commandscore, "r");
    if(file == NULL){
        return -1;
    }
    fscanf(file, "%d", &fifthscore);
    
    pclose(file);

	if(file == NULL){
		return -1;
	}
	return fifthscore;
}

int get_line_number(char* path){
	char* commandlinenum;
	FILE* file;
	int numlines;

	commandlinenum = malloc(sizeof(char)*64);
	sprintf(commandlinenum, "wc -l %s |cut -d\\  -f1", path);
	file = popen(commandlinenum, "r");

	fscanf(file,"%d", &numlines);

	pclose(file);

	return numlines;
}

int get_score_of(char* nom, char* path){
	FILE* file;
    FILE* cutfile;
	char* commandcreatefile;
	char* commandgrep;
	char* commandcut;
	int namefound;
	int score;

	score = -1;

	file = fopen(path, "r");
    if(file == NULL){
        commandcreatefile = malloc(sizeof(char)*30);
        sprintf(commandcreatefile, "touch %s", path);
        system(commandcreatefile);
        return -1;
    }

	commandgrep = malloc(sizeof(char)*128);
	sprintf(commandgrep, "cat %s |grep '%s'", path, nom);
	// printf("Commandgrep : %s\n", commandgrep);
	namefound = system(commandgrep);

	if(namefound == -1 || WEXITSTATUS(namefound) != 0) {
	} else{
		commandcut = malloc(sizeof(char)*128);
		sprintf(commandcut, "%s |cut -d\\  -f1", commandgrep);
		cutfile = popen(commandcut, "r");
		fscanf(cutfile, "%d", &score);
		// printf("Score trouvé pour %s est de %d\n", nom, score);

		pclose(cutfile);
	}
	
	return score;
}

/**
 * @brief Créer le fichier scores_nb et scores_multi_nb s'ils n'existent pas
 * 
 * @param numniv Numéro du niveau
 */
void create_scores_files(int numniv){
	char* bestscore;
	char* scores;
	char* commandcreatebest;
	char* commandcreatescores;

    scores = malloc(sizeof(char)*64);
	sprintf(scores, "scores/scores_%d", numniv);
    bestscore = malloc(sizeof(char)*64);
	sprintf(bestscore, "scores/scores_multi_%d", numniv);

    commandcreatebest = malloc(sizeof(char)*64);
	sprintf(commandcreatebest, "touch %s", bestscore);
    commandcreatescores = malloc(sizeof(char)*64);
	sprintf(commandcreatescores, "touch %s", scores);

	system(commandcreatescores);
	system(commandcreatebest);
}