#include "main.h"

int main(int argc, char const *argv[])
{
	int c;
	int selection = 1;
	int minselection = 1;
	int maxselection = 3;
	
	while (c != '\n' || selection != 3) {
		display_main_menu(selection);
		c = getch();
		if(c == '\n'){
			if(selection == 1){
				play();
			}else if(selection == 2){
				selection = 1;
				maxselection = nb_files("niveaux/") + 1;
				while(c != '\n' || selection != maxselection){
					display_leaderboard_menu(selection);
					c = getch();
					if(c == '\n' && selection != maxselection){
						display_leaderboard(selection);
					}
					if (c % 64 == 1 && selection != minselection) selection--;
					if (c % 64 == 2 && selection != maxselection) selection++;
				}
				selection=1;
				maxselection = 3;
			}
		}

		if (c % 64 == 1 && selection != minselection) selection--;
        if (c % 64 == 2 && selection != maxselection) selection++;
	}
	clearscreen();
	printf("\e[?25h \e[1 q");


	return 0;
}
