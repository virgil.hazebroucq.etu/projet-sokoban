#include "main.h"

niveau_t* nouveau_niveau(int nb_colonnes, int nb_lignes){
	niveau_t* niv;
	niv = malloc(sizeof(niveau_t));
	niv->terrain = calloc(nb_colonnes*nb_lignes, sizeof(char));
	niv->nb_colonnes = nb_colonnes;
	niv->nb_lignes = nb_lignes;
	niv->nbpas = 0;
	return niv;
}

void place_sur_terrain (niveau_t* niveau, int colonne, int ligne, char car){
	*(niveau->terrain + ligne*niveau->nb_colonnes+colonne) = car;
}

char lecture_du_terrain (niveau_t* niveau,int colonne,int ligne){
	return *(niveau->terrain + ligne*niveau->nb_colonnes+colonne);
}

void initialise_le_terrain (niveau_t* niveau){
	// printf("Nb colonnes : %d \n",niveau->nb_colonnes);
	// printf("Nb lignes : %d \n",niveau->nb_lignes);
	for(int idx=0; idx<(niveau->nb_colonnes*niveau->nb_lignes); idx++){
		niveau->terrain[idx] = '#';
	}
}

void move_car(niveau_t* niveau, int cinit, int linit, int cfutur, int lfutur){
	char carinit = lecture_du_terrain(niveau, cinit, linit);
	if(carinit == '+' || carinit == '@') {
		move_player(niveau, cfutur, lfutur);
		return;
	}
	if(car_on_storage(niveau, cinit, linit) == 1){ //Si le car est initialement sur un stockage
		place_sur_terrain(niveau, cinit, linit, '.'); //Change le car à la position initiale du car en .
	}else{ //S'il n'était pas sur un stockage
		place_sur_terrain(niveau, cinit, linit, ' '); //Change le car à la position initiale du car en espace
	}

	if(lecture_du_terrain(niveau, cfutur, lfutur) == '.'){ //Si la position où le car veut se déplacer est un stockage
		place_sur_terrain(niveau, cfutur, lfutur, '*'); //Change le car à la position future du car en +
	} else { //Si elle ne l'est pas
		place_sur_terrain(niveau, cfutur, lfutur, '$'); //Change le car à la position future du car en @
	}
}

/**
 * @brief Vérifie si le caractère à la position donnée est sur un stockage
 * 
 * @param niveau le niveau
 * @param colonne N° de la colonne où se trouve le car
 * @param ligne N° de la ligne où se trouve le car
 * 
 * @return Renvoie 1 si le car est sur un stockage et 0 dans le cas contraire
 */
int car_on_storage(niveau_t* niveau, int colonne, int ligne){
	char car = lecture_du_terrain(niveau, colonne, ligne);
	int onstorage = 0;
	if(car == '+' || car == '*') onstorage = 1;
	return onstorage;
}

void affichage_niveau (niveau_t* niveau){
	for(int l=0; l<(niveau->nb_lignes); l++){
		for(int c=0; c<(niveau->nb_colonnes); c++){
			printf("%c",lecture_du_terrain(niveau, c, l));
		}
		printf("\n");
	}
	printf("\n");
}

niveau_t* lecture_du_niveau(int niveau){
	
	char* nivpath;

	FILE* fichier;

	int nb_colonnes, nb_lignes;

	niveau_t* newniv;
	int i = 0;
	char car = ' ';

	nivpath = malloc(sizeof(char)*64);
	sprintf(nivpath, "niveaux/niveau_%d", niveau);

	fichier = fopen(nivpath, "r");
	if(fichier == NULL) {
		printf("Ce niveau n'existe pas\n");
		return NULL;
	}
	
	fscanf(fichier, "%d %d", &nb_colonnes, &nb_lignes);
	newniv = nouveau_niveau(nb_colonnes, nb_lignes);

	while ( !feof(fichier) ) {
		car = fgetc( fichier );
		if(car != '\n') {
			*(newniv->terrain + i) = car;
			i++;
		}
	}

	newniv->perso = init_player(newniv);
	fclose(fichier);

	free(nivpath);

	return newniv;
}

void liberation_du_niveau (niveau_t* niveau){
	free(niveau);
}

int niveau_termine (niveau_t* niveau) {
	int termine = 1;
	int idx = 0;
	while(termine == 1 && idx<(niveau->nb_colonnes*niveau->nb_lignes)){
		if(niveau->terrain[idx] == '$') termine = 0; 
		idx++;
	}
	return termine;
}