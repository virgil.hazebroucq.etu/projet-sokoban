#include "main.h"

void display_main_menu(int i) {
	clearscreen();
	printf("\e[?25l");
	printf("╭─│ Sokoban │───────────╮\n");
	printf("│\n");
	if (i == 1) {
		printf("│  \033[4;37mJouer\033[0m\n");
	} else {
		printf("│  Jouer\n");
	}

	if (i == 2) {
		printf("│  \033[4;37mLeaderboard\033[0m\n");
	} else {
		printf("│  Leaderboard\n");
	}

	if (i == 3) {
		printf("│  \033[4;37mQuitter\033[0m\n");
	} else {
		printf("│  Quitter\n");
	}
	printf("│\n");
	printf("╰───────────────────────╯\n");
}

void display_leaderboard_menu(int select){
	FILE* file;
	char* command;
	char* line;
	int max;
	int i;

	max = nb_files("niveaux/");

	command = malloc(sizeof(char) * 32);
	sprintf(command, "ls niveaux/");
	file = popen(command, "r");

	line = malloc(sizeof(char) * 32);

	clearscreen();
	printf("\e[?25l");
	printf("╭─│ LeaderBoards │──────╮\n");
	printf("│\n");
	if(file){
		i = 1;
		while(fgets(line, sizeof(line) * 32, file)){
			if(i == select) printf("│  \033[4;37m%s\033[0m", line);
			else printf("│  %s", line);
			i++;
		}
	}

	if (select == max + 1) {
		printf("│  \033[4;37mQuitter\033[0m\n");
	} else {
		printf("│  Quitter\n");
	}
	printf("│\n");
	printf("╰───────────────────────╯\n");
}

void display_leaderboard(int select){
	FILE* file;
	FILE* numfile;
	char* command;
	char* commandls;
	char* line;
	char* scorepath;
	char* joueur;
	int numniv;
	int pts;
	int nblines;

	command = malloc(sizeof(char) * 64);
	sprintf(command, "ls niveaux/ |sed '%dq;d'|cut -d_ -f2", select);
	numfile = popen(command, "r");
	fscanf(numfile, "%d", &numniv);

	create_scores_files(numniv);

	joueur = malloc(sizeof(char) * 32);
	line = malloc(sizeof(int) + sizeof(char) * 32);

	clearscreen();
	scorepath = malloc(sizeof(char) * 32);
	sprintf(scorepath, "scores/scores_multi_%d", numniv);
	// printf("Path : %s\n", scorepath);

	nblines = get_line_number(scorepath);

	// printf("%d\n", nblines);
	commandls = malloc(sizeof(char) * 64);
	sprintf(commandls, "cat %s", scorepath);
	file = popen(commandls, "r");

	printf("\e[?25l");
	if(nblines > 0){
		printf("╭─│ Niveau %d │───────────────────────╮\n", numniv);
		printf("│                                    │\n");
		printf("│      \033[4;37m Joueur  ││    Points \033[0m\t     │\n");
		while (fgets(line, sizeof(line) * 32, file)) {
			sscanf(line, "%d %s", &pts, joueur);
			printf("│      %s\t││\t%d\t     │\n", joueur, pts);
		}
		printf("│                                    │\n");
		printf("╰────────────────────────────────────╯\n");
	}else{
		printf("╭─│ Niveau %d │──────────────────────────────╮\n", numniv);
		printf("│                                           │\n");
		printf("│ Aucune entrée enregistrée pour ce niveau  │\n");
		printf("│                                           │\n");
		printf("╰───────────────────────────────────────────╯\n");
	}
	
	pclose(numfile);
	pclose(file);

	free(joueur);
	free(scorepath);
	
	wait_for_enter();
}

void display_niveau_menu(int select){
	FILE* file;
	char* command;
	char* line;
	int max;
	int i;

	max = nb_files("niveaux/");

	command = malloc(sizeof(char) * 32);
	sprintf(command, "ls niveaux/");
	file = popen(command, "r");

	line = malloc(sizeof(char) * 32);

	clearscreen();
	printf("\e[?25l");
	printf("╭─│ Niveaux │───────────╮\n");
	printf("│\n");
	if(file){
		i = 1;
		while(fgets(line, sizeof(line) * 32, file)){
			if(i == select) printf("│  \033[4;37m%s\033[0m", line);
			else printf("│  %s", line);
			i++;
		}
	}

	if (select == max + 1) {
		printf("│  \033[4;37mQuitter\033[0m\n");
	} else {
		printf("│  Quitter\n");
	}
	printf("│\n");
	printf("╰───────────────────────╯\n");
}

int nb_files(char* path){
	FILE* file;
	char* commandmax;
	int max;

	commandmax = malloc(sizeof(char) * 64);
	sprintf(commandmax, "ls %s | wc -l", path);
	file = popen(commandmax, "r");

	fscanf(file, "%d", &max);

	pclose(file);
	free(commandmax);

	return max;
}

void play(void){
	niveau_t* niveau;
	historique_t* histoire;
	FILE* numfile;
	char* command; 
	char* nom;
	char direction;
	int numniv;
	int maxscore;
	int score;
	int c;
	int selection;
	int minselection;
	int maxselection;

	
	c= ' ';
	minselection = 1;
	selection = 1;
	maxselection = nb_files("niveaux/") + 1;


	printf("\e[?25h \e[1 q\n");
	
	clearscreen();
	while(c != '\n' || selection >= maxselection){
		display_niveau_menu(selection);
		c = getch();
		if(c == '\n' && selection == maxselection){
			return;
		}
		if (c % 64 == 1 && selection != minselection) selection--;
		if (c % 64 == 2 && selection != maxselection) selection++;
	}

	command = malloc(sizeof(char) * 64);
	sprintf(command, "ls niveaux/ |sed '%dq;d'|cut -d_ -f2", selection);
	numfile = popen(command, "r");
	fscanf(numfile, "%d", &numniv);

	niveau = lecture_du_niveau(numniv);
	histoire = nouveau_historique();
	init_historique(histoire, niveau);

	// Fais jouer le joueur jusqu'à ce que le niveau soit fini
	do{
		clearscreen();
		affichage_niveau(niveau); //Affiche l'état du niveau actuel
		printf("Quelle direction voulez-vous prendre ? ");
		direction = entree_du_joueur(); //Demande une position au joueur
		printf("\n%c\n", direction);
		if(direction == 'a'){
			niveau = coup_precedent(histoire);
		}else {
			sauvegarde_un_coup(histoire, niveau);
			deplacement(niveau, direction); //Déplace le joueur dans la direction voulue
			niveau->nbpas += 1; //Augmente le compte du nombre de pas
		}
	}	while(niveau_termine(niveau) != 1);

	clearscreen();

	score = niveau->nbpas;

	sauvegarde_un_coup(histoire, niveau);
	
	// printf("\n\n\n\n\nAffichage de l'historique :\n");
	// affiche_historique(histoire);

	affichage_niveau(niveau); //Affiche le niveau final
	printf("Wow ! Tu as réussi en %d pas !\n", niveau->nbpas);

	maxscore = lecture_du_score(numniv);
	if(maxscore == -1 || maxscore == 0){
		nom = nom_du_joueur();
		clearscreen();
		printf("Tu es le tout premier score enregistré !\nTu as donc le meilleur score !\n");
		ecriture_du_score(numniv, score);
		ecriture_du_score_multi(numniv, score, nom);
	}else if(score < maxscore){
		nom = nom_du_joueur();
		clearscreen();
		printf("Meilleure score du niveau : %d\n", maxscore);
		printf("Bien joué %s, tu as battu le meilleur score !\n", nom);
		ecriture_du_score(numniv, score);
		ecriture_du_score_multi(numniv, score, nom);
	}else if(in_top_5(numniv, score) == 1){
		nom = nom_du_joueur();
		clearscreen();
		printf("Meilleure score du niveau : %d\n", maxscore);
		printf("Bien joué %s, tu es dans le top 5 des meilleurs scores !\n", nom);
		ecriture_du_score_multi(numniv, score, nom);
	} else {
		nom = malloc(sizeof(char)); //Sinon le free(nom) fait crash c:
		printf("Score à battre pour être premier : %d\n", maxscore);
	}

	free(niveau);
	free(histoire);
	free(nom);

	wait_for_enter();
}


void clearscreen(void) {
	system("clear"); 
}
void wait_for_enter(void){
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
	printf("\e[?25l");
	printf("\nAppuyez sur entrée pour continuer...\n");
	while(getchar() != '\n');
	printf("\e[?25h \e[1 q\n");
}

int getch(void) {
	int ch;
	struct termios oldt;
	struct termios newt;

	tcgetattr(STDIN_FILENO, &oldt);   /*store old settings */
	newt = oldt;                      /* copy old settings to new settings */
	newt.c_lflag &= ~(ICANON | ECHO); /* make one change to old settings in new settings */

	tcsetattr(STDIN_FILENO, TCSANOW, &newt); /*apply the new settings immediatly */

	ch = getchar(); /* standard getchar call */

	tcsetattr(STDIN_FILENO, TCSANOW, &oldt); /*reapply the old settings */

	return ch; /*return received char */
}