#include "main.h"

point_t* init_player(niveau_t* niveau){
	point_t* perso;
	int* pos;

	perso = malloc(sizeof(point_t));
	pos = find_pos(niveau);
	perso->colonne = *(pos + 0);
	perso->ligne = *(pos + 1);

	return perso;	
}

int* find_pos(niveau_t* niveau) {
	char car;
	int* pos = malloc(sizeof(int));
	for(int y = 0; y<(niveau->nb_lignes); y++){
		for (int x = 0; x < (niveau->nb_colonnes); x++){
			car = lecture_du_terrain(niveau, x,y);
			if(car == '@' || car == '+'){
				*(pos + 0) = x;
				*(pos + 1) = y;
			} 
		}
	}
	return pos;
}

/**
 * @brief Déplace le joueur à une position donnée
 * 
 * @param niveau Le niveau
 * @param colonne La colonne voulue
 * @param ligne La ligne voulue
 */
void move_player(niveau_t* niveau, int colonne, int ligne){
	point_t* perso = niveau->perso;
	int pos[2] = {perso->colonne, perso->ligne};
	if(player_on_storage(niveau) == 1){ //Si le joueur est initialement sur un stockage
		place_sur_terrain(niveau, pos[0], pos[1], '.'); //Change le car à la position initiale du joueur en .
	}else{ //S'il n'était pas sur un stockage
		place_sur_terrain(niveau, pos[0], pos[1], ' '); //Change le car à la position initiale du joueur en espace
	}

	if(lecture_du_terrain(niveau, colonne, ligne) == '.'){ //Si la position où le joueur veut se déplacer est un stockage
		place_sur_terrain(niveau, colonne, ligne, '+'); //Change le car à la position future du joueur en +
	} else { //Si elle ne l'est pas
		place_sur_terrain(niveau, colonne, ligne, '@'); //Change le car à la position future du joueur en @
	}
	
	update_player_pos(niveau);
}

void update_player_pos(niveau_t* niveau){
	int* pos;
	point_t* perso;
	pos = find_pos(niveau);
	perso = niveau->perso;

	perso->colonne = *(pos + 0);
	perso->ligne = *(pos + 1);
}

int player_on_storage(niveau_t* niveau){
	int onstorage = 0;
	point_t* perso = niveau->perso;
	if(lecture_du_terrain(niveau, perso->colonne, perso->ligne) == '+') onstorage = 1;
	return onstorage;
}