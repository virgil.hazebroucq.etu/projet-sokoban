point_t* init_player(niveau_t* niveau);
int* find_pos(niveau_t* niveau);
void move_player(niveau_t* niveau, int colonne, int ligne);
void update_player_pos(niveau_t* niveau);
int player_on_storage(niveau_t* niveau);