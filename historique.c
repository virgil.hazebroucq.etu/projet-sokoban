#include "main.h"

niveau_t* copie_du_niveau (niveau_t* niveau){
	niveau_t* copie = nouveau_niveau(niveau->nb_colonnes, niveau->nb_lignes);
	copie->nbpas = niveau->nbpas;
	for(int idx = 0; idx<(copie->nb_colonnes*copie->nb_lignes); idx++){
		copie->terrain[idx] = niveau->terrain[idx];
	}
	copie->perso = init_player(copie);
	return copie;
}

historique_t* nouveau_historique(){
	historique_t* histoire;
	histoire = malloc(sizeof(historique_t));
	histoire->tabHistorique = malloc(sizeof(niveau_t));
	histoire->nbNiveau = 0;
	return histoire;
}

void init_historique(historique_t* histoire, niveau_t* niveau){
	histoire->tabHistorique[0] = niveau;
	histoire->nbNiveau = 0;
}

void affiche_historique(historique_t* histoire){
	for(int idx=0; idx<histoire->nbNiveau; idx++){
		// printf("Copie N°%d\n", idx);
		affichage_niveau(histoire->tabHistorique[idx]);
	}
}

void lib_historique(historique_t* histoire){
	for(int idx=0; idx<histoire->nbNiveau; idx++){
		free(histoire->tabHistorique[idx]);
		// printf("Copie N°%d libérée\n", idx);
	}
	free(histoire);
}

void sauvegarde_un_coup (historique_t* histoire, niveau_t* niveau){
    // printf("Nouveau coup sauvegardé\n");
	// printf("Nombre de niveaux dans histoire avant realloc: %d\n", histoire->nbNiveau);
    niveau_t** nouveau = realloc(histoire->tabHistorique, (++histoire->nbNiveau) * sizeof(niveau_t*));
	// printf("tabHistorique reallocated");
	// printf("Nombre de pas niveau paramètre: %d\n", niveau->nbpas);
	// printf("Nombre de niveaux dans histoire: %d\n", histoire->nbNiveau);
    
    nouveau[histoire->nbNiveau-1] = copie_du_niveau(niveau);

	histoire->tabHistorique = nouveau;
}

niveau_t* coup_precedent (historique_t* histoire){
	if(histoire->nbNiveau < 1) return histoire->tabHistorique[histoire->nbNiveau];
	niveau_t* dernier = histoire->tabHistorique[(histoire->nbNiveau)-1];
	// niveau_t** nouveau = malloc(sizeof(histoire->tabHistorique)-sizeof(niveau_t*));
	// for(int idx=0; idx<(histoire->nbNiveau); idx++){
	// 	histoire->tabHistorique[idx] = nouveau[idx]; 
	// }
	histoire->nbNiveau--;
	return dernier;
}