#include "main.h"

char entree_du_joueur (void){
    int verif = 0;
    char saisie;
    while(verif==0){
        scanf("%c", &saisie);
        if(saisie == 'z' || saisie == 'q' || saisie == 's' || saisie == 'd' || saisie == 'a'){
            verif = 1;
        }
    }
    return saisie;
}

void deplacement (niveau_t* niveau, char direction){
    int type = verification_deplacement(niveau, direction);
    if(type == 0){
        printf("Déplacement impossible!\n");
        return;
    } else if(type == 1){
        preparation_deplacement(niveau, direction, type);
    } else {
        preparation_deplacement(niveau, direction, type);
        preparation_deplacement(niveau, direction, type-1);
    }
}

int verification_deplacement(niveau_t* niveau, char direction) { //boolean qui verifie si le deplacement dans la direction passer en parametre est possible
    int verif = 0;
    char emplacement;
    int colonne = niveau->perso->colonne;
    // printf("Colonne du perso : %d\n", colonne);
    int ligne = niveau->perso->ligne;
    // printf("Ligne du perso : %d\n", ligne);
    switch(direction){
        case 'z' :
            if(ligne == 0) return verif;
            emplacement = lecture_du_terrain(niveau, colonne, ligne-1);
            break;
        case 'q' :
            if(colonne == 0) return verif;
            emplacement = lecture_du_terrain(niveau, colonne-1, ligne);
            break;
        case 's' :
            if(ligne == niveau->nb_lignes-1) return verif;
            emplacement = lecture_du_terrain(niveau, colonne, ligne+1);
            break;
        case 'd' :
            if(colonne == niveau->nb_colonnes-1) return verif;
            emplacement = lecture_du_terrain(niveau, colonne+1, ligne);
            break;
    }
    printf("Emplacement : %c\n", emplacement);
    if(emplacement == ' ' || emplacement == '.') {
        verif = 1;
    } else if(emplacement == '$' || emplacement == '*') {
        char emplacement2;
        switch(direction){
            case 'z' :
                if(ligne < 2) return 0;
                emplacement2 = lecture_du_terrain(niveau, colonne, ligne-2);
                break;
            case 'q' :
                if(colonne < 2) return 0;
                emplacement2 = lecture_du_terrain(niveau, colonne-2, ligne);
                break;
            case 's' :
                if(ligne+2 >= niveau->nb_lignes) return 0;
                emplacement2 = lecture_du_terrain(niveau, colonne, ligne+2);
                break;
            case 'd' :
                if(colonne+2 >= niveau->nb_colonnes) return 0;
                emplacement2 = lecture_du_terrain(niveau, colonne+2, ligne);
                break;
        }
        if(emplacement2 == ' ' || emplacement2 == '.'){
            verif = 2;
        }
    }

    return verif;
}

void preparation_deplacement(niveau_t* niveau, char direction, int type) { //deplace le joueur dans la direction demande, et change l'apparence si besoin (fait appel a la fonction change_player_char())
    int colonne = niveau->perso->colonne;
    int ligne = niveau->perso->ligne;
    int nextColonne = colonne;
    int nextLigne = ligne;
    if(type==1){
        switch(direction){
        case 'z' :
            nextLigne -=1;
            break;
        case 'q' :
            nextColonne -=1;
            break;
        case 's' :
            nextLigne +=1;
            break;
        case 'd' :
            nextColonne +=1;
            break;
        }
    } else {
        switch(direction){
        case 'z' :
            ligne -= 1;
            nextLigne -=2;     
            break;
        case 'q' :
            colonne -= 1;
            nextColonne -=2;
            break;
        case 's' :
            ligne += 1;
            nextLigne +=2;
            break;
        case 'd' :
            colonne += 1;
            nextColonne +=2;
            break;
        }
    }
    
    // printf("Type: %d\nPosition actuelle : x:%d y:%d\nPosition future : x:%d y:%d\n", type, colonne, ligne, nextColonne, nextColonne);
    move_car(niveau, colonne, ligne, nextColonne, nextLigne);
}