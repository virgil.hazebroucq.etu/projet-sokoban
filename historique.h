niveau_t* copie_du_niveau (niveau_t* niveau);
historique_t* nouveau_historique();
void init_historique(historique_t* histoire, niveau_t* niveau);
void affiche_historique(historique_t* histoire);
void lib_historique(historique_t* histoire);
void sauvegarde_un_coup (historique_t* hist, niveau_t* niveau);
niveau_t* coup_precedent (historique_t* histoire);