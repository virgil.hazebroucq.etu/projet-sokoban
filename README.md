# Projet Sokoban

Projet Sokoban créé par **Steffe Kélian** et **Hazebroucq Virgil** durant la première année  universitaire du DUT Informatique à l'IUT A de Lille

Description
-
Reprise du jeu **[Sokoban](https://fr.wikipedia.org/wiki/Sokoban)** de *Hiroyuki Imabayashi* entièrement programmé en langage C.

Règles du jeu
-
Gardien d'entrepôt, le joueur doit **ranger des caisses** sur des **cases cibles**.
<br>Il peut **se déplacer dans les quatres directions**, et **pousser une seule caisse à la fois**. 
<br>**Une fois toutes les caisses rangées, le niveau est réussi** et le joueur peut choisir un autre niveau.
<br>L'idéal étant de **réussir avec le moins de coups possibles** (déplacements et poussées).
<br>Si le joueur atteint un nombre de coups record ou le top 5 de ce niveau, il lui est demandé d'entrer un pseudonyme 
<br>afin d'être affiché plus tard dans le Leaderboard du niveau.

Commandes
-
<u>**Dans les menus**</u>

Pour se déplacer dans les menus, il suffit d'appuyer sur les **flèches directionnelles** du clavier afin de naviguer dans le menu.
<br>Appuyer sur *Entrée* pour valider votre sélection.


<u>**En jeu**</u>

Pour se déplacer, le joueur doit entrer les touches *z*, *q*, *s* ou *d* pour se déplacer dans la direction correspondante.
<br>On peut aussi entrer la touche *a* pour annuler le dernier coup effectué. 
<br>Et enfin appuyer sur *Entrée* pour valider le déplacement.
<br>"*z*" pour aller en **avant**.
<br>"*s*" pour aller en **arrière**.
<br>"*q*" pour aller à **gauche**.
<br>"*d*" pour aller à **droite**. 
<br>"*a*" pour **revenir d'un pas**.