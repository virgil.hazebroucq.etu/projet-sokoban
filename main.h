#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <dirent.h>

typedef struct {
	int colonne;
	int ligne;
} point_t;

typedef struct {
	char* terrain;
	int nb_colonnes;
	int nb_lignes;
	point_t* perso;
	int nbpas;
} niveau_t;

typedef struct {
	niveau_t** tabHistorique;
	int nbNiveau;
} historique_t;

#include "terrain.h"
#include "player.h"
#include "deplacement.h"
#include "score.h"
#include "historique.h"
#include "menu.h"